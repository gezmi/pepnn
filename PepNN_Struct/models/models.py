import numpy as np
import torch
import torch.nn as nn
from .layers import *
from .modules import *

def to_var(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return x



def get_neighbor_features(nodes, neighbor_idx):
    
    '''
    function from Ingraham et al. 2019 source code

    '''

    # Features [B,N,C] at Neighbor indices [B,N,K] => [B,N,K,C]
    # Flatten and expand indices per batch [B,N,K] => [B,NK] => [B,NK,C]
    neighbors_flat = neighbor_idx.view((neighbor_idx.shape[0], -1))
    neighbors_flat = neighbors_flat.unsqueeze(-1).expand(-1, -1, nodes.size(2))
    # Gather and re-pack
    neighbor_features = torch.gather(nodes, 1, neighbors_flat)
    neighbor_features = neighbor_features.view(list(neighbor_idx.shape)[:3] + [-1])
    return neighbor_features

def cat_neighbors_nodes(nodes, neighbors, neighbor_indices):
    
    nodes = get_neighbor_features(nodes, neighbor_indices)
    edge_features = torch.cat([neighbors, nodes], -1)
    
    return edge_features




class RepeatedModule(nn.Module):
    
    def __init__(self, edge_features, node_features, n_layers, d_model,
                 n_head, d_k, d_v, d_inner, dropout=0.1):
        
        super().__init__()
        
        self.edge_embedding = nn.Linear(edge_features, d_model)
        self.node_embedding = nn.Linear(node_features, d_model)
        self.sequence_embedding = nn.Embedding(20, d_model)
        self.d_model = d_model 
        
        self.reciprocal_layer_stack = nn.ModuleList([
                ReciprocalLayer(d_model,  d_inner,  n_head, d_k, d_v, dropout=dropout) 
                for _ in range(n_layers)])
    
        self.dropout = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)
        self.dropout_3 = nn.Dropout(dropout)
    
    
    def _positional_embedding(self, batches, number):
        
        result = torch.exp(torch.arange(0, self.d_model,2,dtype=torch.float32)*-1*(np.log(10000)/self.d_model))
        
        numbers = torch.arange(0, number, dtype=torch.float32)
        
        numbers = numbers.unsqueeze(0)
        
        numbers = numbers.unsqueeze(2)
       
        result = numbers*result
        
        result = torch.cat((torch.sin(result), torch.cos(result)),2)
       
        return result
    
    def forward(self, peptide_sequence, nodes, edges, neighbor_indices):
        
        
        sequence_attention_list = []
        
        graph_attention_list = []
        
        graph_seq_attention_list = []
        
        seq_graph_attention_list = []
        
        sequence_enc = self.sequence_embedding(peptide_sequence)
        
        
       
        sequence_enc += to_var(self._positional_embedding(peptide_sequence.shape[0],
                                                           peptide_sequence.shape[1]))    
        sequence_enc = self.dropout(sequence_enc)
        
        
        
        
        encoded_edges = self.edge_embedding(edges)
        encoded_nodes = self.node_embedding(nodes)
        
        node_enc = encoded_nodes
        
        edge_input = cat_neighbors_nodes(encoded_nodes, encoded_edges, 
                                         neighbor_indices)
        
        node_enc = self.dropout_2(node_enc)
        
        edge_input = self.dropout_3(edge_input)

        for reciprocal_layer in self.reciprocal_layer_stack:
            
            node_enc, sequence_enc, graph_attention, sequence_attention, node_seq_attention, seq_node_attention =\
                reciprocal_layer(sequence_enc, node_enc, edge_input)
            
            sequence_attention_list.append(sequence_attention)
            
            graph_attention_list.append(graph_attention)
            
            graph_seq_attention_list.append(node_seq_attention)
            
            seq_graph_attention_list.append(seq_node_attention)
            
            edge_input = cat_neighbors_nodes(node_enc, encoded_edges,
                                             neighbor_indices)
        
        
        return node_enc, sequence_enc, sequence_attention_list, graph_attention_list,\
            seq_graph_attention_list, graph_seq_attention_list
    

class FullModel(nn.Module):
    
    def __init__(self, edge_features, node_features, n_layers, d_model, n_head,
                 d_k, d_v, d_inner, dropout=0.1, return_attention=False):
        
        super().__init__()
        self.repeated_module = RepeatedModule(edge_features, node_features, n_layers, d_model,
                               n_head, d_k, d_v, d_inner, dropout=dropout)
        
        self.final_attention_layer = MultiHeadAttentionSequence(n_head, d_model,
                                                                d_k, d_v, dropout=dropout)
        
        self.final_ffn = FFN(d_model, d_inner, dropout=dropout) 
        self.output_projection_nodes = nn.Linear(d_model, 2)
        
        
        
        self.softmax_nodes =nn.LogSoftmax(dim=-1)
   
                
        self.return_attention = return_attention
        
    def forward(self, peptide_sequence, nodes, edges, neighbor_indices):
        
      
        node_enc, sequence_enc, sequence_attention_list, graph_attention_list,\
            seq_graph_attention_list, graph_seq_attention_list = self.repeated_module(peptide_sequence,
                                                                                     nodes,
                                                                                     edges,
                                                                                     neighbor_indices)
        
        
        node_enc, final_node_seq_attention  = self.final_attention_layer(node_enc, sequence_enc, sequence_enc)
        
        node_enc = self.final_ffn(node_enc)

        node_enc = self.softmax_nodes(self.output_projection_nodes(node_enc))
        
        
        
        
        
        if not self.return_attention:
            return node_enc
        else:
            return node_enc, sequence_attention_list, graph_attention_list,\
            seq_graph_attention_list, graph_seq_attention_list
        
